use actix_web::{error, Result};

pub type Pool = r2d2::Pool<r2d2_sqlite::SqliteConnectionManager>;

trait ToActixError<T> {
    fn map_500(self) -> actix_web::Result<T>;
}

impl <T, E: std::fmt::Debug + std::fmt::Display + 'static> ToActixError<T> for std::result::Result<T, E> {
    fn map_500(self) -> actix_web::Result<T> {
        self.map_err(|e| error::ErrorInternalServerError(e))
    }
}

pub async fn get_pool() -> Pool {
    let manager = r2d2_sqlite::SqliteConnectionManager::file("db.sqlite3");
    let pool = r2d2::Pool::new(manager).unwrap();
    pool
}

pub async fn create_logs_table(pool: &Pool) -> Result<()> {
    let pool = pool.clone();
    let conn = pool.get().map_500()?;

    let mut stmt = conn.prepare("create table if not exists logs (id integer primary key, msg text not null)").map_500()?;
    let _ = stmt.execute([]);

    Ok({})
}

pub async fn create_log_entry(pool: &Pool, msg: String) -> Result<()> {
    let pool = pool.clone();
    let conn =  pool.get().map_500()?;

    let mut stmt = conn.prepare("insert into logs (msg) values (?)").map_500()?;
    let _ = stmt.execute(&[&msg]);

    Ok({})
}

pub async fn get_log_entry_count(pool: &Pool) -> Result<i32> {
    let pool = pool.clone();
    let conn = pool.get().map_500()?;

    let mut stmt = conn.prepare("select count(*) from logs").map_500()?;

    stmt.query_row([], |row| row.get(0)).map_500()
}