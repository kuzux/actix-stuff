use actix_web::{Responder, HttpResponse, get, HttpServer, web, Result};
use serde::Serialize;

mod db;

#[derive(Serialize, Debug)]
struct Thingy {
    name: String,
    age: u8,
}

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

#[get("/greet/{name}")]
async fn greet(path: web::Path<String>) -> Result<String> {
    let name = path.into_inner();
    Ok(format!("Hello, {}!", &name))
}

#[get("/jsml")]
async fn jsml() -> Result<impl Responder> {
    let thingy = Thingy {
        name: "Bob".to_string(),
        age: 42,
    };
    Ok(web::Json(thingy))
}

#[get("/log/{msg}")]
async fn add_log(pool: web::Data<db::Pool>, path: web::Path<String>) -> Result<impl Responder> {
    let msg = path.into_inner();
    db::create_log_entry(&pool, msg).await.unwrap();
    Ok(HttpResponse::Ok().body("OK"))
}

#[get("/logs/count")]
async fn get_log_count(pool: web::Data<db::Pool>) -> Result<impl Responder> {
    let count = db::get_log_entry_count(&pool).await.unwrap();
    Ok(HttpResponse::Ok().body(format!("{}", count)))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let pool = db::get_pool().await;
    db::create_logs_table(&pool).await.unwrap();

    HttpServer::new(move || {
        actix_web::App::new()
            .app_data(web::Data::new(pool.clone()))
            .service(hello)
            .service(greet)
            .service(jsml)
            .service(add_log)
            .service(get_log_count)
    }).bind(("127.0.0.1", 4321))?
        .run()
        .await
}
